<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    protected $fillable = ['name', 'admin'];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
