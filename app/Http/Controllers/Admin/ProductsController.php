<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Category;
use App\Http\Requests\Product\StoreProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Product;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('admin.products.index')->with(compact('products'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('admin.products.create')->with(compact('categories'));
    }

    public function store(StoreProductRequest $request)
    {
        Product::create($request->all());
        return redirect()->route('admin.products.index')->with('success', 'Created successfully');
    }

    public function edit(Product $product)
    {
        $categories = Category::all();
        return view('admin.products.edit', compact(['product', 'categories']));
    }

    public function update(Product $product, UpdateProductRequest $request)
    {
        $product->update($request->all());
        return redirect()->route('admin.products.index')->with('success', 'Updated successfully');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('admin.products.index')->with('success', 'Removed successfully');
    }
}
