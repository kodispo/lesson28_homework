<?php

namespace App\Http\Controllers;

use App\Product;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('products.index')->with(compact('products'));
    }

    public function show(Product $product)
    {
        return view('products.show')->with(compact('product'));
    }
}
