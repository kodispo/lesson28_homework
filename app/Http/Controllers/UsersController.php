<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\SignUpUserRequest;
use App\User;
use App\UserGroup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function signUp()
    {
        return view('users.sign-up');
    }

    public function store(SignUpUserRequest $request)
    {
        DB::transaction(function() use ($request) {
            $user = User::create($request->all(['email','name']) + ['password' => bcrypt($request->post('password'))]);
            Auth::login($user);

            $group = UserGroup::where('name', 'client')->first();
            $user->groups()->attach($group);
        });

        return redirect()->route('home');
    }
}
