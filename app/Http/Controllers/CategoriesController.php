<?php

namespace App\Http\Controllers;

use App\Category;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('categories.index')->with(compact('categories'));
    }

    public function show(Category $category)
    {
        return view('categories.show')->with(compact('category'));
    }
}
