<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\SignInUserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' =>'destroy']);
        $this->middleware('auth',['only' => 'destroy']);
    }

    public function destroy()
    {
        if (Auth::check()){
            Auth::logout();
        }
        return redirect()->route('login');
    }

    public function create()
    {
        return view('users.sign-in');
    }

    public function store(SignInUserRequest $request)
    {
        if (Auth::attempt($request->all(['email', 'password']))) {
            if (Auth::user()->isAdmin()) {
                return redirect()->route('admin.dashboard');
            } else {
                return redirect()->route('home');
            }
        } else {
            return back();
        }
    }
}
