<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            [
                'customer_name' => 'Nell Cohen',
                'email' => 'cohen@gmail.com',
                'phone' => '+1-202-555-0111',
                'feedback' => 'Nelson the Apple employee that helped me fix my daughter\'s iPad was extremely patient, innovative in his approach to solving the problem and dedicated to stick with it until the issue was solved.',
            ],
            [
                'customer_name' => 'Sally Mckee',
                'email' => 'mckee@gmail.com',
                'phone' => '+1-202-555-0189',
                'feedback' => 'Apple continues to deliver wonderful and innovative consumer products. The new ECG apple watch feature is revolutionary for personal healthcare technology.',
            ],
            [
                'customer_name' => 'Lynn Andersen',
                'email' => 'andersen@gmail.com',
                'phone' => '+1-202-555-0185',
                'feedback' => 'I had an awesome experience at the Apple store buying my iPhone 11 Pro Max and AirPods Pro. The employees were super helpful and friendly and made it easy for me to complete my purchase. Apple is the best smartphone company period. The best company of all time.',
            ],
        ]);
    }
}
