<?php

use Illuminate\Database\Seeder;
use App\User;
use App\UserGroup;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->addUser('Aadam Fleming', 'admin@test.com', '111111', 'admin');
        $this->addUser('Tobey Laing', 'editor@test.com', '111111', 'editor');
        $this->addUser('Timur Cole', 'client@test.com', '111111', 'client');
    }

    protected function addUser($name, $email, $password, $role)
    {
        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password),
        ]);
        $group = UserGroup::where('name', $role)->first();
        $user->groups()->attach($group);
    }
}
