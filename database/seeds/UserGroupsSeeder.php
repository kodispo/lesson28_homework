<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_groups')->insert([
            [
                'name' => 'admin',
                'admin' => 1,
            ],
            [
                'name' => 'editor',
                'admin' => 1,
            ],
            [
                'name' => 'client',
                'admin' => 0,
            ],
        ]);
    }
}
