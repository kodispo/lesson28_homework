@extends('layout.base')

@section('hero')
    <h1 class="text-uppercase">{{ $product->title }}</h1>
    <p class="small">
        @if($product->category)
            <a href="/categories/{{ $product->category->slug }}">{{ $product->category->name }}</a>
        @else
            Uncategorized
        @endif
    </p>
    <p class="h3">${{ $product->price }}</p>
@endsection

@section('content')
    {{ $product->description }}
@endsection
