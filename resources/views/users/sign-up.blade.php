@extends('layout.base')

@section('hero')
    <h1 class="text-uppercase">Sign up</h1>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-6">
            @include('partials.errors')
            <form action="/sign-up" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="text" name="email" id="email" value="{{ old('email') }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" name="password" id="password" value="{{ old('password') }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="password_confirmation">Password Conformation:</label>
                    <input type="password" name="password_confirmation" id="password_confirmation"
                           value="{{ old('password_confirmation') }}" class="form-control">
                </div>

                <div class="form-group">
                    <button class="btn btn-success">Sign up</button>
                </div>
            </form>
        </div>
    </div>
@endsection
