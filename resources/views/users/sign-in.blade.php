@extends('layout.base')

@section('hero')
    <h1 class="text-uppercase">Sign in</h1>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-6">
            @include('partials.errors')
            <form action="/sign-in" method="post">
                @csrf
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="text" name="email" id="email" value="{{ old('email') }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" name="password" id="password" value="{{ old('password') }}" class="form-control">
                </div>

                <div class="form-group">
                    <button class="btn btn-success">Sign in</button>
                </div>
            </form>
        </div>
    </div>
@endsection
