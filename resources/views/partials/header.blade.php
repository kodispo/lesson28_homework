<div class="bg-white border-bottom box-shadow">
    <div class="container d-flex flex-column flex-md-row align-items-center p-3 px-md-4">
        <h5 class="my-0 mr-md-auto font-weight-normal"><a class="text-dark logo" href="/">Lesson 28</a></h5>
        <nav class="my-2 my-md-0 mr-md-3 d-flex">
            <a class="p-2 text-dark" href="/">Products</a>
            <a class="p-2 text-dark" href="/categories/">Categories</a>
            <a class="p-2 text-dark" href="/pages/">Pages</a>
            @guest
                <a class="p-2 text-dark" href="/sign-up">Sign up</a>
                <a class="p-2 text-dark" href="/sign-in">Sign in</a>
            @endguest
            @auth
                <form action="/logout" method="post">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-outline-danger">Logout ({{Auth::user()->name}})</button>
                </form>
            @endauth
        </nav>
    </div>
</div>
