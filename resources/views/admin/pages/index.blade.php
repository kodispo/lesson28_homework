@extends('admin.layout.base')

@section('top')
    <h1 class="h2">Pages</h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <a href="{{ route('admin.pages.create') }}" class="btn btn-sm btn-outline-success">Add new</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Slug</th>
                    <th>Intro</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($pages as $page)
                    <tr>
                        <td>{{ $page->id }}</td>
                        <td>{{ $page->title }}</td>
                        <td>{{ $page->slug }}</td>
                        <td>{{ $page->intro }}</td>
                        <td>
                            <div class="btn-group">
                                <a target="_blank" href="{{ route('pages.show', $page->slug) }}" class="btn btn-sm btn-outline-primary">View on site</a>
                                <a href="{{ route('admin.pages.edit', $page->slug) }}" class="btn btn-sm btn-outline-primary ml-1">Edit</a>
                                <form action="{{ route('admin.pages.destroy', $page->slug) }}" method="post" class="d-block">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-sm btn-outline-danger ml-1">Remove</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" class="text-danger">no pages</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection
