@extends('admin.layout.base')

@section('top')
    <h1 class="h2">Edit page</h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @include('partials.errors')
            <form action="{{ route('admin.pages.update', $page->slug) }}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="title">Title:</label>
                    <input type="text" name="title" id="title" value="{{ $page->title }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="slug">Slug:</label>
                    <input type="text" name="slug" id="slug" value="{{ $page->slug }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="intro">Intro:</label>
                    <textarea name="intro" id="intro" class="form-control" rows="5">{{ $page->intro }}</textarea>
                </div>

                <div class="form-group">
                    <label for="content">Content:</label>
                    <textarea name="content" id="content" class="form-control" rows="5">{{ $page->content }}</textarea>
                </div>

                <div class="form-group">
                    <button class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection
