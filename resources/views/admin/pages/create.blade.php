@extends('admin.layout.base')

@section('top')
    <h1 class="h2">Add new page</h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @include('partials.errors')
            <form action="{{ route('admin.pages.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="title">Title:</label>
                    <input type="text" name="title" id="title" value="{{ old('title') }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="slug">Slug:</label>
                    <input type="text" name="slug" id="slug" value="{{ old('slug') }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="intro">Intro:</label>
                    <textarea name="intro" id="intro" class="form-control" rows="5">{{ old('intro') }}</textarea>
                </div>

                <div class="form-group">
                    <label for="content">Content:</label>
                    <textarea name="content" id="content" class="form-control" rows="5">{{ old('content') }}</textarea>
                </div>

                <div class="form-group">
                    <button class="btn btn-success">Add</button>
                </div>
            </form>
        </div>
    </div>
@endsection
