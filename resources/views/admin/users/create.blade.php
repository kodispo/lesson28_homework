@extends('admin.layout.base')

@section('top')
    <h1 class="h2">Add new user</h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @include('partials.errors')
            <form action="{{ route('admin.users.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="text" name="email" id="email" value="{{ old('email') }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="groups">User Groups:</label>
                    <select name="groups" id="groups"  class="form-control" multiple>
                        @foreach($groups as $group)
                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" name="password" id="password" value="{{ old('password') }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="password_confirmation">Password Conformation:</label>
                    <input type="password" name="password_confirmation" id="password_confirmation"
                           value="{{ old('password_confirmation') }}" class="form-control">
                </div>

                <div class="form-group">
                    <button class="btn btn-success">Sign up</button>
                </div>
            </form>
        </div>
    </div>
@endsection
