@extends('admin.layout.base')

@section('top')
    <h1 class="h2">Categories</h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <a href="{{ route('admin.categories.create') }}" class="btn btn-sm btn-outline-success">Add new</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Slug</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($categories as $category)
                    <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->name }}</td>
                        <td>{{ $category->slug }}</td>
                        <td>
                            <div class="btn-group">
                                <a target="_blank" href="{{ route('categories.show', $category->slug) }}" class="btn btn-sm btn-outline-primary">View on site</a>
                                <a href="{{ route('admin.categories.edit', $category->slug) }}" class="btn btn-sm btn-outline-primary ml-1">Edit</a>
                                <form action="{{ route('admin.categories.destroy', $category->slug) }}" method="post" class="d-block">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-sm btn-outline-danger ml-1">Remove</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" class="text-danger">no categories</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection
