@extends('admin.layout.base')

@section('top')
    <h1 class="h2">Edit category</h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @include('partials.errors')
            <form action="{{ route('admin.categories.update', $category->slug) }}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" name="name" id="name" value="{{ $category->name }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="slug">Slug:</label>
                    <input type="text" name="slug" id="slug" value="{{ $category->slug }}" class="form-control">
                </div>

                <div class="form-group">
                    <button class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection
