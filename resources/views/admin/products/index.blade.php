@extends('admin.layout.base')

@section('top')
    <h1 class="h2">Products</h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <a href="{{ route('admin.products.create') }}" class="btn btn-sm btn-outline-success">Add new</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Slug</th>
                    <th>Price</th>
                    <th>Category</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($products as $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->title }}</td>
                        <td>{{ $product->slug }}</td>
                        <td>{{ $product->price }}</td>
                        <td>
                            @if($product->category)
                                <a href="{{ route('categories.show', $product->category->slug) }}" target="_blank">{{ $product->category->name }}</a>
                            @else
                                Uncategorized
                            @endif
                        </td>
                        <td>
                            <div class="btn-group">
                                <a target="_blank" href="{{ route('products.show', $product->slug) }}" class="btn btn-sm btn-outline-primary">View on site</a>
                                <a href="{{ route('admin.products.edit', $product->slug) }}" class="btn btn-sm btn-outline-primary ml-1">Edit</a>
                                <form action="{{ route('admin.products.destroy', $product->slug) }}" method="post" class="d-block">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-sm btn-outline-danger ml-1">Remove</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" class="text-danger">no products</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection
