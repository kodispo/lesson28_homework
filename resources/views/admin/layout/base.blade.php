<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Dashboard</title>
        @include('admin.partials.head')
    </head>

    <body>
        @include('admin.partials.header')

        <div class="container-fluid">
            <div class="row">
                @include('admin.partials.sidebar')
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        @yield('top')
                    </div>
                    @include('admin.partials.alert')
                    @yield('content')
                </main>
            </div>
        </div>

        @include('admin.partials.footer')
    </body>
</html>
