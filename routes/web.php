<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// home page
Route::get('/', "ProductsController@index")->name('home');

// dashboard
Route::prefix('admin')->middleware('admin')->group(function(){
    Route::get('/', 'Admin\DashboardController@index')->name('admin.dashboard');
    Route::resource('/pages', "Admin\PagesController", ['except' => ['show'], 'as' => 'admin']);
    Route::resource('/users', "Admin\UsersController", ['except' => ['show'], 'as' => 'admin']);
    // shop
    Route::resource('/products', "Admin\ProductsController", ['except' => ['show'], 'as' => 'admin']);
    Route::resource('/categories', "Admin\CategoriesController", ['except' => ['show'], 'as' => 'admin']);
});

// sign up
Route::get('/sign-up', "UsersController@signUp");
Route::post('/sign-up', "UsersController@store");

// sign in
Route::get('/sign-in', "LoginController@create")->name('login');
Route::post('/sign-in', "LoginController@store");
Route::delete('/logout', "LoginController@destroy");

Route::resource('products', "ProductsController", ['only' => ['index', 'show']]);
Route::resource('categories', "CategoriesController", ['only' => ['index', 'show']]);
Route::get('{page}', "PagesController@show")->name('pages.show');
